document.addEventListener('DOMContentLoaded', () => {
	const canvas = document.getElementsByTagName('canvas')[0];
	const ctx = canvas.getContext('2d');

	const [w, h] = [800, 800];
	const toRad = Math.PI / 180;

	let mx = 0;
	let my = 0;
	let loop;

	ctx.fillStyle = 'black';
	ctx.fillRect(0, 0, w, h);

	ctx.translate(w / 2, h / 2);

	if (loop) clearInterval(loop);
	else loop = setInterval(paint, 1000 / 120);

	ctx.scale(2, 2);
	// TODO: Call paint 4x with rotations in between instead of individual functions inside?
	function paint() {
		// ctx.fillStyle = 'steelblue';
		// ctx.fillStyle = '#c09f7b';

		ctx.fillStyle = 'black';
		ctx.fillRect(-w / 2, -h / 2, w, h);
		
		ctx.strokeStyle = '#de819dee';
		ctx.lineWidth = 4;

		function circle (x, y) {
			ctx.beginPath();
			ctx.arc(x, y, 30, 0, 2 * Math.PI);
			ctx.stroke();
		}

		circle(0, -142, 15);
		circle(0, 142, 15);
		circle(-142, 0, 15);
		circle(142, 0, 15);

		function miniTri (rotDeg = 0) {
			const rot = rotDeg % 360;
			ctx.rotate(rot * toRad);

			ctx.fillStyle = '#de819dee';

			const hw = 4; // half-width of tri
			const th = 25; // tri height
			const vy = -114; // valley y

			ctx.beginPath();
			ctx.moveTo(-hw, vy);
			ctx.lineTo(0, vy + th);
			ctx.lineTo(hw, vy);
			ctx.arcTo(hw, vy, -hw, vy, th);
			ctx.fill();
			ctx.rotate(-rot * toRad);
		}

		miniTri();
		miniTri(90);
		miniTri(180);
		miniTri(-90);

		ctx.lineWidth = 2;

		ctx.strokeStyle = '#FFF5FF';
		ctx.fillStyle = ctx.strokeStyle;

		ctx.scale(0.5, 0.5);
		ctx.fillText(`${mx}, ${my}`, - w / 2 + 10, - h / 2 + 15);
		ctx.scale(2, 2);

		function petal (x, y, rotDeg = 0) {
			const rot = rotDeg % 180;
			ctx.translate(x, y);
			ctx.rotate(rot * toRad);
			ctx.translate(-x, -y);

			ctx.beginPath();
			ctx.moveTo(x, y - 60);
			ctx.arcTo(x + 45, y, x, y + 60, 100);
			ctx.arcTo(x - 45, y, x, y - 60, 100);
			ctx.shadowBlur = 5;
			ctx.shadowColor = ctx.fillStyle;
			ctx.fill();

			ctx.translate(x, y);
			ctx.rotate(-rot * toRad);
			ctx.translate(-x, -y);
		}

		ctx.rotate(Math.PI / 4);
		petal(0, -70); // Top
		petal(0, 70); // Bottom
		petal(-70, 0, 90); // Left
		petal(70, 0, 90); // Right
		ctx.rotate(-Math.PI / 4);

		ctx.shadowBlur = 0;

		function miniArrow (x, y, rotDeg = 0) {
			const rot = rotDeg % 360;
			ctx.translate(x, y);
			ctx.rotate(rot * toRad);
			ctx.translate(-x, -y);

			const aw = 8; // arrow width
			const ah = 24; // arrow height

			ctx.beginPath();
			// Top right corner
			ctx.moveTo(x + aw, y - ah);
			// Right bezier down to middle
			ctx.bezierCurveTo(x + aw/2, y - ah/2, x + aw, y - ah/2, x, y);
			// Middle bezier up to top left
			ctx.bezierCurveTo(x - aw, y - ah/2, x - aw/2, y - ah/2, x - aw, y - ah); // 

			// top left to middle curve
			// ctx.arcTo(x - aw/2, y - ah/2, x, y - ah, 4.1);
			ctx.bezierCurveTo(x - aw/2, y - ah/2, x, y - ah, x, y - ah - 3);

			// middle to top right curve
			// ctx.arcTo(x + aw/2, y - ah/2, x + aw, y - ah, 4.1); 
			ctx.bezierCurveTo(x, y - ah, x + aw/2, y - ah/2, x + aw, y - ah);

			ctx.fill();
			ctx.closePath();

			ctx.translate(x, y);
			ctx.rotate(-rot * toRad);
			ctx.translate(-x, -y);
		}

		miniArrow(0, -12); // T
		miniArrow(0, 12, 180); // B
		miniArrow(-12, 0, -90); // L
		miniArrow(12, 0, 90); // R

		function miniShaft (rotDeg = 0) {
			const rot = rotDeg % 360;
			ctx.rotate(rot * toRad);

			const shw = 3; // width
			const o = 45; // y offset
			const l = 6; // length

			ctx.moveTo(-shw, -o);
			ctx.lineTo(-shw, -o-l);
			ctx.lineTo(shw, -o-l);
			ctx.lineTo(shw, -o);
			ctx.arc(0, -o, shw, 0, Math.PI);
			ctx.fill();

			ctx.rotate(-rot * toRad);
		}

		miniShaft();
		miniShaft(90);
		miniShaft(180);
		miniShaft(-90);

		function diamond (rotDeg = 0) {
			const rot = rotDeg % 360;
			ctx.rotate(rot * toRad);

			ctx.beginPath();
			ctx.moveTo(0, -48);
			ctx.lineTo(-10, -58);
			ctx.lineTo(0, -84);
			ctx.lineTo(10, -58);
			ctx.fill();

			ctx.rotate(-rot * toRad);
		}

		diamond();
		diamond(90);
		diamond(-90);
		diamond(180);
	}

	canvas.addEventListener('mousemove', e => {
		const r = canvas.getBoundingClientRect();

		mx = e.clientX - r.left;
		my = e.clientY - r.top;
	});
});
