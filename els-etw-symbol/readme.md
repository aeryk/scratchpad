# Elsword - Eternity Winner smybol

Created: 2019-03-07

This is my attempt at recreating the symbol that appears when the class reserves a command activated skill.

## Usage

Open `index.html` in a browser.

## Notes

No measurements were done on the reference images to determine how to accurately draw the graphic.
This has mostly been eyeballing, hence there's a lot of raw values for offsets on positions and bezier curve control points
