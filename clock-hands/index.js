/* eslint-disable semi */
const doubleCircle = 720

// half degrees to not have to deal with floats until the end
let minuteHalfDeg = 0
let hourHalfDeg = 0

while (hourHalfDeg <= doubleCircle) {
	const hour = (Math.floor(hourHalfDeg / 60) + '').padStart(2, '0')
	const minute = (minuteHalfDeg / 12 + '').padStart(2, '0')

	// Difference of degrees is less than half a minute hand's movement
	const diff = Math.abs(hourHalfDeg - minuteHalfDeg)
	if (diff < 6) {
		let out = `${hour}:${minute}`
		if (diff > 0) {
			const sign = minuteHalfDeg > hourHalfDeg ? '+' : '-'
			out += ` (${sign}${(diff / 2).toFixed(1)} deg)`
		}
		console.log(out)
	}
	// Aside: Could add second hand + minute hand moving every 
	// second, for more precise time of aligned min + hour hand 

	hourHalfDeg++
	minuteHalfDeg += 12
	minuteHalfDeg %= doubleCircle
}