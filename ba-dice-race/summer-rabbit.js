/**
 * Blue Archive - Dice Race minigame simulator
 * Event: Summer Special Operation! RABBIT Squad adn the Mystery of the Missing Shrimp
 * 
 * Created: 2023-12-04
 * Updated: 2023-12-31
 */

/** @type {[number, string][]} */
const pool = [
	[5, 'eleph'],
	[17, 'act'],
	[2, 'cp'],
	[10, 'enh'],
	[1, 'grt'],
	[4, 'eleph'],
	[6, 'eligma'],
	[10, 'act'],
	[12, 'enh'],
	[1.6, 'cp'],
	[6, 'shift'],
	[2.4, 'cp'],
	[12, 'eligma'],
	[7, 'eleph'],
	[8, 'eligma'],
	[22, 'act'],
	[15, 'enh'],
	[3.2, 'cp']
];

const res = {
	grt: 0,
	eleph: 0,
	eligma: 0,
	enh: 0,
	act: 0,
	cp: 0
};

let pos = 0;
let laps = 0;
const lapGoal = 20;
const rollCost = 500;
let rolls = 0;

while (laps < lapGoal) {
	rolls++;

	const diceRoll =
		Math.ceil(Math.random() * 6); // average case
	
	// Run
	if (pos + diceRoll >= pool.length) {
		laps++;
		pos = (pos + diceRoll) % pool.length;
	} else {
		pos += diceRoll;
	}

	let [ amount, name ] = pool[pos];

	// Land on shift
	if (name === 'shift') {
		pos += amount;

		if (pos >= pool.length) {
			laps++;
			pos %= pool.length;
		}

		[ amount, name ] = pool[pos]; // update reference
	}

	res[name] += amount;
}

console.log(`Dice Rolls: ${rolls} (${rolls * rollCost} Frozen Shrimp)`);
console.log(`Laps: ${laps}\n`);

console.log(`Rewards: (Excluding Lap Rewards)`);
console.log(`${res.grt}x Guaranteed Roll Ticket`);
console.log(`${res.eleph}x Miyu (Swimsuit)'s Eleph`);
console.log(`${res.eligma}x Eligma`);
console.log(`${res.enh}x Advanced Enhancement Stone`);
console.log(`${res.act}x Advanced Activity Report`);
console.log(`${res.cp.toFixed(1)}m Credit Points`); // funny thing can happen without toFixed()
