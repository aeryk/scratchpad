# Blue Archive - Dice Race simulator

This is quickly made simulator to satisfy my curiosity.

## Dates

Sorted by Event Date

| Event Name                                            | Event Date (JP) | Script Created |
| ----------------------------------------------------- | --------------- | -------------- |
| Get Set, Go!                                          | 2022-10-26      | 2023-05-05     |
| RABBIT Squad and the<br>Mystery of the Missing Shrimp | 2023-06-21      | 2023-12-04     |
| Get Set, Go! (Rerun)                                  | 2023-09-15      | 2023-09-29     |

## Usage

The file can run by itself using Node.js, or attached to a `<script>` tag in a HTML file. The output is in the console.

### Non-rerun only

The following are some changes you can make in your local copy of the code to fit your need:

- Change `lapGoal` from 20 to whatever integer. The real thing only rewards reaching up to 50 laps.
- Check the commented out pre-determined dice rolls to see the maximum amount of a specific reward you can get within the lap goal.
