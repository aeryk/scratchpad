/**
 * Blue Archive - Dice Race minigame simulator
 * Event: Get Set, Go! (Rerun)
 * 
 * Created: 2023-09-29
 * Updated: 2023-12-31
 */

/** @type {[number, string][]} */
const pool = [
	[1, 'grt'],
	[53, 'eqp'],
	[13, 'enh'],
	[17, 'act'],
	[3, 'shift'],
	[2, 'cp'],
	[41, 'eqp'],
	[11, 'act'],
	[5, 'eligma'],
	[1.6, 'cp'],
	[1, 'shift'],
	[1.8, 'cp'],
	[4, 'eligma'],
	[11, 'enh'],
	[37, 'eqp'],
	[1.9, 'cp'],
	[2, 'shift'],
	[19, 'act']
];

const res = {
	grt: 0,
	eqp: 0,
	eligma: 0,
	enh: 0,
	act: 0,
	cp: 0
};

let pos = 0;
let laps = 0;
const lapGoal = 20;
const rollCost = 500;
let rolls = 0;

while (laps < lapGoal) {
	rolls++;

	const diceRoll =
		Math.ceil(Math.random() * 6); // average case
	
	// Run
	if (pos + diceRoll >= pool.length) {
		laps++;
		pos = (pos + diceRoll) % pool.length;
	} else {
		pos += diceRoll;
	}

	let [ amount, name ] = pool[pos];

	// Land on shift
	if (name === 'shift') {
		pos += amount;

		if (pos >= pool.length) {
			laps++;
			pos %= pool.length;
		}

		[ amount, name ] = pool[pos]; // update reference
	}

	res[name] += amount;
}

console.log(`Dice Rolls: ${rolls} (${rolls * rollCost} Sports Drink)`);
console.log(`Laps: ${laps}\n`);

console.log(`Rewards: (Excluding Lap Rewards)`);
console.log(`${res.grt}x Guaranteed Roll Ticket`);
console.log(`${res.eligma}x Eligma`);
console.log(`${res.eqp}x Equipment Box(?)`);
console.log(`${res.enh}x Advanced Enhancement Stone`);
console.log(`${res.act}x Advanced Activity Report`);
console.log(`${res.cp.toFixed(1)}m Credit Points`); // funny thing can happen without toFixed()
