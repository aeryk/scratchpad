/**
 * Blue Archive - Dice Race minigame simulator
 * Event: Get Set, Go!
 * 
 * Created: 2023-05-05
 * Updated: 2023-09-29
 */

/** @type {[number, string][]} */
const pool = [
	[1, 'eleph'],
	[1, 'eligma'],
	[6, 'enh'],
	[6, 'act'],
	[3, 'shift'],
	[0.8, 'cp'],
	[1, 'eleph'],
	[0.5, 'cp'],
	[4, 'enh'],
	[1.2, 'cp'],
	[1, 'shift'],
	[4, 'act'],
	[1, 'eligma'],
	[2, 'act'],
	[2, 'enh'],
	[3, 'eleph'],
	[2, 'shift'],
	[2, 'eleph']
];

const res = {
	eleph: 0,
	eligma: 0,
	enh: 0,
	act: 0,
	cp: 0
};

let pos = 0;
let laps = 0;
const lapGoal = 50;
const rollCost = 150;
let rolls = 0;

while (laps < lapGoal) {
	rolls++;

	const diceRoll =
		Math.ceil(Math.random() * 6); // average case
		// comment the line above ^ before trying one below
		// unrealistic cases:
		// [4, 6, 3][rolls % 3]; // or [4, 6, 5]; least roll case, like [6, 6, 6]
		// [4, 6, 2, 2, 1][rolls % 5]; // max eleph case
		// [1, 5, 6, 6][rolls % 4]; // or [1, 6, 5, 4]; max eligma case
		// [3, 6, 2, 2, 5][rolls % 5] // or [3, 6, 1, 2, 3]; max activity report case
		// [5, 2, 2, 6, 3][rolls % 5]; // or [5, 2, 2, 6, 1]; max credit case
	
	// Run
	if (pos + diceRoll >= pool.length) {
		laps++;
		pos = (pos + diceRoll) % pool.length;
	} else {
		pos += diceRoll;
	}

	let [ amount, name ] = pool[pos];

	// Land on shift
	if (name === 'shift') {
		pos += amount;

		if (pos >= pool.length) {
			laps++;
			pos %= pool.length;
		}

		[ amount, name ] = pool[pos]; // update reference
	}

	res[name] += amount;
}

console.log(`Dice Rolls: ${rolls} (${rolls * rollCost} Sports Drink)`);
console.log(`Laps: ${laps}\n`);

console.log(`Rewards: (Excluding Lap Rewards)`);
console.log(`${res.eleph}x Hasumi (Track)'s Eleph`);
console.log(`${res.eligma}x Eligma`);
console.log(`${res.enh}x Advanced Enhancement Stone`);
console.log(`${res.act}x Advanced Activity Report`);
console.log(`${res.cp.toFixed(1)}m Credit Points`); // funny thing can happen without toFixed()
