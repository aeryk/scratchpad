// LeetCode #8 https://leetcode.com/problems/string-to-integer-atoi

var myAtoi = function(s) {
    const ans = s.replace(/^ *([-+]?\d*).*/g, '$1')
    let ansNum = Number(ans) || 0
    if (ansNum < -(2 ** 31)) ansNum = -(2 ** 31)
    if (ansNum > 2 ** 31 - 1) ansNum = 2 ** 31 - 1
    return ansNum
};
// Solution End

console.log(myAtoi('42'))
console.log(myAtoi('   -42'))
console.log(myAtoi('4193 with words'))