// LeetCode #2631 https://leetcode.com/problems/group-by/

Array.prototype.groupBy = function(fn) {
    const groups = {}
    for (const v of this) {
        const k = fn(v);
        if (groups[k] == undefined) {
            groups[k] = []
        }
        groups[k].push(v)
    }
    return groups
};
// Solution End

const t1 = [
    {"id":"1"},
    {"id":"1"},
    {"id":"2"}
]
console.log(t1.groupBy(i => i.id))

const t2 = [
    [1, 2, 3],
    [1, 3, 5],
    [1, 5, 9]
]
console.log(t2.groupBy(l => String(l[0])))

const t3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(t3.groupBy(n => n > 5))