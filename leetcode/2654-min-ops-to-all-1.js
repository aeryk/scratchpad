/**
 * LeetCode #2654 https://leetcode.com/problems/minimum-number-of-operations-to-make-all-array-elements-equal-to-1/
 * 
 * @param {number[]} nums
 * @return {number}
 */
var minOperations = function (nums) {
    let n = nums.length
    if (nums.includes(1)) {
        return n - nums.filter(a => a === 1).length
    }

    let res = 1e6 + 1
    for (let i = 0; i < n; i++) {
        let g = nums[i]
        for (let j = i + 1; j < n; j++) {
            g = gcd(g, nums[j])
            if (g === 1) {
                res = Math.min(res, j - i + n - 1)
                break
            }
        }
    }
    return res === 1e6 + 1 ? -1 : res
};

function gcd(a, b) {
    if (a === 0) return b
    if (b === 0) return a

    let az = ctz(a)
    const bz = ctz(b)
    const s = Math.min(az, bz)
    b >>= bz

    while (a !== 0) {
        a >>= az
        let d = b - a
        az = ctz(d)
        b = Math.min(a, b)
        a = Math.abs(d)
    }

    return b << s
}

// assumptions: already n > 0
function ctz(n) {
    return 31 - Math.clz32(n & -n)
}
// Solution End
console.log(minOperations([2, 6, 3, 4]) === 4)

console.log(minOperations([2, 10, 6, 14]) === -1)