const _ = require('lodash')

// LeetCode #2120 https://leetcode.com/problems/execution-of-all-suffix-instructions-staying-in-a-grid/

var executeInstructions = function(n, startPos, s) {
    let curPos = startPos.slice() // copy
    const moveMap = {
        U: () => curPos[0]--,
        D: () => curPos[0]++,
        L: () => curPos[1]--,
        R: () => curPos[1]++
    }
    let out = []
    for (let st = 0; st < s.length; st++) {
        const ss = s.substring(st)
        curPos = startPos.slice() // reset
        for (let i = 0; i < ss.length; i++) {
            moveMap[ss[i]]() // move
            const validY = curPos[0] >= 0 && curPos[0] < n
            const validX = curPos[1] >= 0 && curPos[1] < n
            if (validX && validY) {
                if (i === ss.length - 1) {
                    out.push(i + 1)
                } else continue
            } else {
                out.push(i)
                break
            }
        }
    }
    return out
};
// Solution End

console.log(executeInstructions(2, [1, 1], 'LURD'))
console.log(executeInstructions(1, [0, 0], 'LRUD'))