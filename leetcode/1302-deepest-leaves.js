// LeetCode #1302 https://leetcode.com/problems/deepest-leaves-sum/

var deepestLeavesSum = function(root) {
    let deepest = 0
    let sum = 0
    function searchBranch(node, depth) {
        if (node.left != null)
            searchBranch(node.left, depth + 1)

        if (node.right != null)
            searchBranch(node.right, depth + 1)
            
        if (node.left == null && node.right == null) {
            if (depth === deepest) {
                sum += node.val
            } else if (depth > deepest) {
                sum = node.val
                deepest = depth
            }
        }
    }
    searchBranch(root, 0)
    return sum
};
// Solution End

// build tree by level order
function toTree(arr, i = 0) {
    let root = null
    if (i < arr.length) {
        root = { val: arr[i] }

        // declutter by checking boundary
        // prevents having nodes with null in all properties
        if (2 * i + 1 < arr.length)
            root.left = toTree(arr, 2 * i + 1)

        if (2 * i + 2 < arr.length)
            root.right = toTree(arr, 2 * i + 2)
    }
    return root
}

const t1 = toTree([1,2,3,4,5,null,6,7,null,null,null,null,8])
console.log(deepestLeavesSum(t1))

const t2 = toTree([6,7,8,2,7,1,3,9,null,1,4,null,null,null,5])
console.log(deepestLeavesSum(t2))