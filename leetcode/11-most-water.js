// LeetCode #11 https://leetcode.com/problems/container-with-most-water/

var maxArea = function(height) {
    let maxSq = 0
    function squish(l, r) {
        const sq = Math.min(height[l], height[r]) * (r - l)
        if (sq > maxSq)
            maxSq = sq

        if (height[l] < height[r]) {
            if (l + 1 < r)
                squish(l + 1, r)
        } else {
            if (l < r - 1)
                squish(l, r - 1)
        }
    }
    squish(0, height.length - 1)
    return maxSq
};
// Solution End

console.log(maxArea([1,8,6,2,5,4,8,3,7]))
console.log(maxArea([1,1]))