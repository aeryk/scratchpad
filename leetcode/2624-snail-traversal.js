// LeetCode #2624 https://leetcode.com/problems/snail-traversal/

Array.prototype.snail = function (rowsCount, colsCount) {
    if (rowsCount * colsCount !== this.length) return []

    if (rowsCount === 1) return this

    const out = Array(rowsCount).fill(0).map(() => [])

    for (let i = 0; i < this.length; i++) {
        const col = (i / rowsCount) | 0
        const row = col % 2 === 1 ? rowsCount - 1 - i % rowsCount : i % rowsCount
        out[row][col] = this[i]
    }

    return out
}
// Solution End

const t1 = [19, 10, 3, 7, 9, 8, 5, 2, 1, 17, 16, 14, 12, 18, 6, 13, 11, 20, 4, 15]
console.log(t1.snail(5, 4))
console.log([1, 2, 3, 4].snail(1, 4))
console.log([1, 3].snail(2, 2))
