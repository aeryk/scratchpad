// LeetCode #2633 https://leetcode.com/problems/convert-object-to-json-string/

var jsonStringify = function(object) {
    if (Object(object) !== object)
        return object
    else {
        let out = ''
        if (Array.isArray(object)) {
            out += `[${object.map(v => jsonStringify(v)).join(',')}]`
        } else {
            out += `{${Object.entries(object).map((k, v) => `"${k}":${jsonStringify(v)}`)}}`
        }
        return out
    }
};
// Solution End

console.log(jsonStringify({"y":1,"x":2}))
console.log(jsonStringify({"a":"str","b":-12,"c":true,"d":null}))
console.log(jsonStringify({"key":{"a":1,"b":[{},null,"Hello"]}}))
console.log(jsonStringify(true))