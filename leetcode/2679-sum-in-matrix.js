const _ = require('lodash')

/**
 * LeetCode #2679 https://leetcode.com/problems/sum-in-a-matrix/
 * 
 * @param {number[][]} nums
 * @return {number}
 */
var matrixSum = function(nums) {
    for (let ri in nums) {
        nums[ri].sort((a, b) => b - a)
    }
    // sort, transpose, then sum of each row, no reduce
    return _.unzip(nums).map(r => Math.max(...r)).reduce((a, b) => a + b)

    /*
	// sort, transpose, then sum of each row
	let score = 0
    
    _.unzip(nums).forEach(r => score += Math.max(...r))

    return score
	*/
};
// Solution End

console.log(matrixSum([[7,2,1],[6,4,2],[6,5,3],[3,2,1]]) === 15)
console.log(matrixSum([[1]]) === 1)