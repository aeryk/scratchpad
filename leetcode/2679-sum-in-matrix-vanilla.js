/**
 * LeetCode #2679 https://leetcode.com/problems/sum-in-a-matrix/
 * 
 * @param {number[][]} nums
 * @return {number}
 */
var matrixSum = function(nums) {
    let score = 0
    let tally = []
    let firstRun = true
    while (nums[0].length > 0) {
        for (let ri in nums) {
            if (firstRun) nums[ri].sort((a, b) => b - a)
            tally.push(nums[ri].pop())
        }
        firstRun = false

        score += Math.max(...tally)
        tally = []
    }
    return score
};
// Solution End

console.log(matrixSum([[7,2,1],[6,4,2],[6,5,3],[3,2,1]]) === 15)
console.log(matrixSum([[1]]) === 1)